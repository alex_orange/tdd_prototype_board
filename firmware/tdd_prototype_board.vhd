library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tdd_prototype_board is
    port
    (
        PB    : in std_logic_vector(4 to 5);
        LED : out std_logic_vector(2 to 3);
        
        SDR_GPIO : inout std_logic_vector(11 downto 0);
        FRONTEND_GPIO : inout std_logic_vector(15 downto 0);
        
        CLK_STANDBY : out std_logic;
        CLK_10MHz : in std_logic;
        REF_10_MHz : in std_logic;
        PPS_IN : in std_logic;
        
        PA_EN : out std_logic;
        PA_REG_EN : out std_logic;
        PA_REG_PS_SYNC : out std_logic;
        PA_REG_VSEL : out std_logic_vector(1 to 2);
        PA_REG_DCDC_PG : in std_logic;
        PA_REG_LDO_PG : in std_logic;
        
        LNA_BIAS : out std_logic_vector(3 downto 0);
        LNA_REG_EN : out std_logic;
        LNA_REG_PS_SYNC : out std_logic;
        LNA_REG_VSEL : out std_logic_vector(1 to 2);
        LNA_REG_DCDC_PG : in std_logic;
        LNA_REG_LDO_PG : in std_logic;
        
        TDD_ENABLE : out std_logic;
        TDD_CTRL : out std_logic;
        
        A_5VA_EN : out std_logic;
        A_5VA_PG : in std_logic;
        
        ANALOG_SEL : out std_logic_vector(1 to 2);
        REF_EN : out std_logic;

        -- Power supply sync signals (FPGA power supply?)
        SYNC : out std_logic_vector(3 downto 0);
        
        ATT_SI : out std_logic;
        ATT_CLK : out std_logic;
        ATT_LE : out std_logic;
        
        RF_REG_EN : out std_logic;
        RF_REG_PS_SYNC : out std_logic;
        RF_REG_DCDC_PG : in std_logic;
        RF_REG_LDO_4_8_PG : in std_logic;
        RF_REG_LDO_3_3VRF_PG : in std_logic;
        
        FAN_PWN : out std_logic;
        FAN_TACH : in std_logic;
        
        MCU_IO : inout std_logic_vector(35 downto 0)
    );
end tdd_prototype_board;

architecture simple of tdd_prototype_board is
    signal master_clk_10mhz : std_logic;
    signal power_good : std_logic;
    signal clk_5mhz : std_logic;

    signal radio_rx_indication : std_logic;

    signal signal_LNA_on : std_logic;
    signal signal_PA_on : std_logic;
    signal signal_SW_rx : std_logic;
    signal signal_SW_tx : std_logic;
begin
    master_clk_10mhz <= ref_10_mhz;

    -- LED(2) <= PB(4);
    -- LED(3) <= PB(5);
    RF_REG_EN <= '1';
    PA_REG_EN <= '1';
    LNA_REG_EN <= '1';
    
    LNA_BIAS <= "1111" when signal_LNA_on = '0' else
                "1000";
    PA_EN <= signal_PA_on;
    
    PA_REG_VSEL <= "00";
    LNA_REG_VSEL <= "00";
    
    TDD_ENABLE <= '1';
    TDD_CTRL <= signal_SW_tx;
    
    CLK_STANDBY <= '0';
    
    power_good <= (RF_REG_LDO_4_8_PG
        AND RF_REG_LDO_3_3VRF_PG
        AND RF_REG_DCDC_PG
        AND PA_REG_DCDC_PG
        AND PA_REG_LDO_PG
        AND LNA_REG_DCDC_PG
        AND LNA_REG_LDO_PG);
    
    clock_process:
    process(master_clk_10mhz)
        variable count : integer := 0;
        variable toggle_output : std_logic := '0';
    begin
        if master_clk_10mhz'event and master_clk_10mhz = '1' then
            if count = 10000000 then
                toggle_output := not toggle_output;
                count := 0;
            end if;
            
            count := count + 1;
            
            LED(2) <= toggle_output AND power_good;
            LED(3) <= toggle_output;
        end if;
    end process clock_process;
    
    five_mhz_process:
    process(master_clk_10mhz)
        variable state : std_logic := '0';
    begin
        if master_clk_10mhz'event and master_clk_10mhz = '1' then
            state := not state;
            clk_5mhz <= state;
        end if;
    end process;
    
    attenuator_process:
    process(clk_5mhz)
        variable bit_sequence : std_logic_vector(0 to 33) :=
        --   0123456789012345601234567890123456
            "0000000010000000000000010000000000";
        variable le_sequence : std_logic_vector(0 to 33) :=
        --   0123456789012345601234567890123456
            "0000000000000000100000000000000001";
        variable clk_en : std_logic := '1';

        -- Using this number because when I used 100 the attenuator wasn't
        -- getting consistent values on startup. Also didn't have default clk
        -- disable so it's possible it was coming in/out of power good after
        -- 100 cycles of power good, not sure. This value together with clk_en
        -- default disable fixes the problem.
        constant wait_init : integer := 1000;
        variable wait_count : integer := wait_init;
    begin
        if clk_5mhz'event and clk_5mhz = '0' then
            if power_good /= '1' then
                -- TODO: Reinitialize the bit sequence vectors and start over
                -- in this case. Should make this significantly more robust.
                wait_count := wait_init;
            else
                if wait_count > 0 then
                    wait_count := wait_count - 1;
                end if;
            end if;

            clk_en := '0';

            if wait_count = 0 then
                ATT_SI <= bit_sequence(0);
                ATT_LE <= le_sequence(0);

                clk_en := '1';

                if le_sequence(0) = '1' then
                    clk_en := '0';
                end if;
                
                bit_sequence(0 to 32) := bit_sequence(1 to 33);
                bit_sequence(33) := '0';
                le_sequence(0 to 32) := le_sequence(1 to 33);
                le_sequence(33) := '0';

                if le_sequence = "0000000000000000000000000000000000" then
                    clk_en := '0';
                end if;
            end if;
        end if;

        ATT_CLK <= clk_5mhz and clk_en;
    end process;

    SDR_GPIO <= (4 => 'Z', others => '0');
    radio_rx_indication <= SDR_GPIO(4);

    break_before_make_proc:
    process(master_clk_10mhz)
        variable count : unsigned(4 downto 0) := to_unsigned(0, 5);
        constant RX_to_TX_LNA_off_to_SW_delay : unsigned(4 downto 0) :=
            to_unsigned(10, 5);
        constant RX_to_TX_SW_to_PA_on_delay : unsigned(4 downto 0) :=
            to_unsigned(5, 5);
        constant TX_to_RX_PA_off_to_SW_delay : unsigned(4 downto 0) :=
            to_unsigned(15, 5);
        constant TX_to_RX_SW_to_LNA_on_delay : unsigned(4 downto 0) :=
            to_unsigned(15, 5);
        variable last_input : std_logic;

        -- RX - steady state RX
        -- TRAN_TO_TX_1 - LNA off, SW still in RX mode
        -- TRAN_TO_TX_2 - LNA off, SW in TX mode, PA still off
        -- TX - steady state TX
        -- TRAN_TO_RX_1 - PA off, SW still in TX mode
        -- TRAN_TO_TX_2 - PA off, SW in RX mode, LNA still off
        type break_state_t is (RX, TRAN_TO_TX, TX, TRAN_TO_RX);
        variable break_state : break_state_t := RX;

        variable sw_in : std_logic := '0';
        variable LNA_on : std_logic := '0';
        variable PA_on : std_logic := '0';
        variable SW_rx : std_logic := '0';
    begin
        if master_clk_10mhz'event and master_clk_10mhz = '1' then
            -- Doing this to handle metastability
            sw_in := last_input;
            last_input := radio_rx_indication;

            case break_state is
                when RX =>
                    PA_on := '0';
                    SW_rx := '1';
                    
                    if sw_in = '1' then
                        LNA_on := '1';
                        count := RX_to_TX_LNA_off_to_SW_delay;
                    else
                        LNA_on := '0';
                        count := count - 1;
                        if count = 0 then
                            break_state := TRAN_TO_TX;
                            count := RX_to_TX_SW_to_PA_on_delay;
                        end if;
                    end if;
                when TRAN_TO_TX =>
                    PA_on := '0';
                    LNA_on := '0';
                    
                    if sw_in = '1' then
                        SW_rx := '1';
                        -- Switch to TRAN_TO_RX and associated delay
                        break_state := TRAN_TO_RX;
                        count := TX_to_RX_SW_to_LNA_on_delay;
                    else
                        SW_rx := '0';
                        count := count - 1;
                        if count = 0 then
                            break_state := TX;
                            count := TX_to_RX_PA_off_to_SW_delay;
                        end if;
                    end if;
                when TX =>
                    SW_rx := '0';
                    LNA_on := '0';
                    
                    if sw_in = '1' then
                        PA_on := '0';
                        count := count - 1;
                        if count = 0 then
                            break_state := TRAN_TO_RX;
                            count := TX_to_RX_SW_to_LNA_on_delay;
                        end if;
                    else
                        PA_on := '1';
                        count := TX_to_RX_PA_off_to_SW_delay;
                    end if;
                when TRAN_TO_RX =>
                    PA_on := '0';
                    LNA_on := '0';
                    
                    if sw_in = '1' then
                        SW_rx := '1';
                        count := count - 1;
                        if count = 0 then
                            break_state := RX;
                            count := RX_to_TX_LNA_off_to_SW_delay;
                        end if;
                    else
                        SW_rx := '0';
                        -- Switch to TRAN_TO_TX and associated delay
                        break_state := TRAN_TO_TX;
                        count := RX_to_TX_SW_to_PA_on_delay;
                    end if;
            end case;

            signal_LNA_on <= LNA_on;
            signal_PA_on <= PA_on;
            signal_SW_rx <= SW_rx;
            signal_SW_tx <= not SW_rx;
        end if;
    end process break_before_make_proc;
end;
